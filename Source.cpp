#include"Vehicle.h"
#include <map>
#include <iostream>
int main()
{
	std::map<Vehicle, std::string, Vehicle::Comparator>cars;

	//Vehicle Dacia("Dacia", 2000);
	//Vehicle Audi("Audi", 1999);

	//cars.emplace(Dacia, "Dacia");
	//cars.emplace(Audi, "Audi");

	cars.emplace(Vehicle("Dacia", 2000), "Dacia");
	cars.emplace(Vehicle("Audi", 2001), "Audi");
	cars.emplace(Vehicle("Renault", 2002), "Renault");
	cars.emplace(Vehicle("Ford", 2003), "Ford");
	cars.emplace(Vehicle("Mitsubishi", 2004), "Mitsubishi");

	for (const auto & element : cars)
	{
		std::cout << element.second << '\n';
	}
	system("pause");
}